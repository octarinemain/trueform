'use strict';

$(document).ready(function() {
	
	function hiddenScroll() {
		if ($(document).height() > $(window).height()) {
			let scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
			$('html').addClass('no-scroll').css('top', -scrollTop);
		}
	}

	function visibleScroll() {
		let scrollTop = parseInt($('html').css('top'));
		$('html').removeClass('no-scroll');
		$('html, body').scrollTop(-scrollTop);
	}


	// Loading
	(function loading() {
		setTimeout(function() {
			$('#menu').removeClass('menu--hidden');
		}, 500);
	
		setTimeout(function() {
			$('body').removeClass('loading');
			$('.popup').removeClass('popup--hidden');
		}, 800);
	}) ();

	
	// Lazy load
	(function lazyLoading() {
		const lazyloadImages = document.querySelectorAll('.lazy');

		if ('IntersectionObserver' in window) {
			let imageObserver = new IntersectionObserver(function(entries) {
				entries.forEach(function(entry) {
					if (entry.isIntersecting) {
						let image = entry.target;
						image.src = image.dataset.src;
						image.classList.add('loaded');
						imageObserver.unobserve(image);
					}
				});
			});

			lazyloadImages.forEach(function(image) {
				imageObserver.observe(image);
			});
		} else { 
			let lazyloadThrottleTimeout;
			
			let lazyload = function() {
				if (lazyloadThrottleTimeout) clearTimeout(lazyloadThrottleTimeout);  

				lazyloadThrottleTimeout = setTimeout(function() {
					let scrollTop = window.pageYOffset;
					lazyloadImages.forEach(function(img) {
						if (img.offsetTop < (window.innerHeight + scrollTop)) {
							img.src = img.dataset.src;
							img.classList.add('loaded');
						}
					});
					if (lazyloadImages.length === 0) { 
						document.removeEventListener('scroll', lazyload);
						window.removeEventListener('resize orientationChange', lazyload);
					}
				}, 20);
			};
			lazyload();

			document.addEventListener('scroll', lazyload);
			window.addEventListener('resize orientationChange', lazyload);
		}
	}) ();

	
	// Init menu
	const initMenu = (function() {
		let menu = $('#menu');
		let logo = $('#logo');
		let desktopBtn = menu.find('.menu-icon.menu-icon--desktop');
		let closeMenu = $('.close-menu');
		let mobileBtn = menu.find('.menu-icon.menu-icon--mobile');
		let header = $('.header').find('.header__inner');
		let contactList = $('#contacts');
		let resizeHover = true;
		let resizeAppend = true;
		let resizeClick = true;
		const popups = $('.popup');

		function toggleClassLogo() {
			if (!popups.hasClass('js-popup-show')) return $(window).scrollTop() > 0 ? logo.addClass('min') : logo.removeClass('min');
			
			return false;	
		}
		toggleClassLogo();

		$(window).on('scroll.logo', toggleClassLogo);

		function hoverClickIn() {
			$(this).closest(menu).addClass('active');
			$(window).off('scroll.logo');
			logo.removeClass('min');
			if (!$('html').hasClass('no-scroll')) hiddenScroll();
			closeMenu.addClass('active');
		}

		function hoverOut() {
			$(this).removeClass('active');
			$(window).on('scroll.logo', toggleClassLogo);
			if ($('html').hasClass('no-scroll')) visibleScroll();
			closeMenu.removeClass('active');
		}

		function clickOut() {
			menu.removeClass('active');
			$(window).on('scroll.logo', toggleClassLogo);
			if ($('html').hasClass('no-scroll')) visibleScroll();
			closeMenu.removeClass('active');
		}

		function clickOnMenuMobile() {
			$(this).closest(menu).toggleClass('active');
			closeMenu.toggleClass('active');
			if ($(this).closest(menu).hasClass('active')) {
				logo.removeClass('min');
				$(window).off('scroll.logo');
				hiddenScroll();
				return;
			}
			$(window).on('scroll.logo', toggleClassLogo);
			if ($('html').hasClass('no-scroll')) visibleScroll();
			return;
		}

		function resizeInitMenu() {
			let maxWidth = window.matchMedia('screen and (max-width: 991px)').matches;

			if (!maxWidth && resizeHover) {
				desktopBtn.on('mouseenter click', hoverClickIn);
				menu.on('mouseleave', hoverOut);
				closeMenu.on('click', clickOut);
				resizeHover = false;
			} else if (maxWidth && !resizeHover) {
				desktopBtn.off('mouseenter click');
				menu.off('mouseleave');
				closeMenu.off('click');
				resizeHover = true;
			}

			if (maxWidth && resizeAppend) {
				logo.after(contactList);
				resizeAppend = false;
			} else if (!maxWidth && !resizeAppend) {
				contactList.appendTo(header);
				resizeAppend = true;
			}

			if (maxWidth && resizeClick) {
				mobileBtn.on('click', clickOnMenuMobile);
				resizeClick = false;
			} else if (!maxWidth && !resizeClick) {
				mobileBtn.off('click');
				resizeClick = true;
			}
		}

		return {
			resizeInitMenu: resizeInitMenu
		};
	}) ();
	
	initMenu.resizeInitMenu();

	// Init filter
	(function initFilter() {
		const menu = $('#menu');
		const navList = menu.find('.nav__list');
		const contactList = menu.find('.contact-list--column');
		const btnToFilter = menu.find('.nav__item--filter a');
		const filter = menu.find('.filter');
		const btnBack = filter.find('.filter__btn-back a');
		const filterList = filter.find('.filter__list');

		if (menu.hasClass('menu--filter')) {
			btnToFilter.on('click', function(e) {
				e.preventDefault();
				menu.addClass('menu--notransition');
				contactList.fadeOut(300);
				navList.fadeOut(300, function() {
					filter.fadeIn(300);
					setTimeout(() => menu.removeClass('menu--notransition'), 300);
				});
			});

			btnBack.on('click', function(e) {
				e.preventDefault();
				menu.addClass('menu--notransition');
				filter.fadeOut(300, function() {
					navList.fadeIn(300);
					contactList.fadeIn(300);
					setTimeout(() => menu.removeClass('menu--notransition'), 300);
				});
			});

			const removeBtnShow = showEl => {
				filterList.find(showEl).each(function() {
					let $this = $(this);
					let items = $this.parent().find('> .js-filter-item');
					
					if (items.length <= 3) $this.remove();
				});				
			};
			removeBtnShow('.filter__show');
			

			filter.on('click', '.filter__subtitle a', function(e) {
				e.preventDefault();
				let $this = $(this);
				let filterList = $this.closest('.filter__box');
				let subList = $this.parent().next();
				
				if ($this.closest('.filter__item').find('.filter__sublist').length <= 0) {
					return window.location = $this.get(0).href;
				}
				
				if (subList.is(':visible')) {
					$this.parent().removeClass('active');
					subList.slideUp('fast');
				} else {
					filterList.find('.filter__subtitle')
						.removeClass('active')
						.siblings('.filter__sublist')
						.slideUp('fast');
					$this.parent().addClass('active');
					subList.slideDown('fast');
				}
			});

			filter.on('click', '.filter__show a', function(e) {
				e.preventDefault();
				let $this = $(this);
				let list = $this.parent().parent();
				let item = list.find('> .js-filter-item:hidden');
				let chank = item.splice(0, 3);
				$(chank).slideDown('fast');
				
				if (item.length === 0) return $this.parent().remove();
			});
		}
	}) ();


	// Init custom Scroll
	(function initScroll() {
		let menu = document.getElementById('menu');
		let navList = menu.querySelector('.nav__list');

		if (Boolean(document.getElementById('filter-inner')) !== Boolean(null)) {
			let filter = document.getElementById('filter-inner');
			initCustomScroll(filter);
		}
		
		initCustomScroll(navList);

		function initCustomScroll(el) {
			new SimpleBar(el, {
				autoHide: false,
				scrollbarMinSize: 50
			});
		}
	}) ();


	// Append production texts
	const appendProdTexts = (function() {
		let textProduction = $('#production').find('.section-production__wrap-text');
		let resizeProduction = true;

		function resizeProdTexts() {
			let maxWidth = window.matchMedia('screen and (max-width: 479px)').matches;
			
			if (maxWidth && resizeProduction) {
				textProduction.each(function() {
					let parent = $(this).closest('.section-production__row');
					parent.append($(this));
				});
				resizeProduction = false;
			} else if (!maxWidth && !resizeProduction) {
				textProduction.each(function() {
					let parent = $(this).closest('.section-production__row').find('.section-production__col-text');
					parent.append($(this));
				});
				resizeProduction = true;
			}
		}

		return {
			resizeProdTexts: resizeProdTexts
		};
	}) ();

	if (document.getElementById('production') !== null)	appendProdTexts.resizeProdTexts();


	// Play videos
	(function playVideos() {
		const videosProduction = $('#production').find('.video');
		
		if (videosProduction.length < 1) return;

		function visibleVideo(el) {
			let eTop = el.offset().top;
			let eBottom = eTop + el.height();
			let wTop = $(window).scrollTop();
			let wBottom = wTop + $(window).height();
			let totalH = Math.max(eBottom, wBottom) - Math.min(eTop, wTop);
			let wComp = totalH - $(window).height();
			let eIn = el.height() - wComp;
			return (eIn <= 0 ? 0 : eIn / el.height() * 100);
		}

		function playVideo() {
			videosProduction.each(function() {
				let $this = $(this);

				if (parseInt(visibleVideo($this)) >= 50) $this[0].play();
				else $this[0].pause();
			});
		}
		playVideo();

		$(window).on('scroll.video resize.video', playVideo);
	}) ();


	// Init sliders
	(function initSliders() {
		let mainSlider = $('.section-slider__main');
		let revSlider = $('#slider-reviews');
		let wrapSliderThumbs = $('#slider-thumbs');	
		let galleryMain = wrapSliderThumbs.find('.gallery-main');
		let galleryMainThumbs = wrapSliderThumbs.find('.gallery-thumbs');
		let galleryMainSlides = galleryMain.find('.swiper-slide').length <= 1;
		let twoProductsSlider = $('#slider-two-products');
		let productsSlider = $('#slider-products');
		let productWatch = $('#slider-products-watch');
		let productWatchYpu = $('#slider-products-watch-you');

		new Swiper(mainSlider, {
			slidesPerView: 4,
			spaceBetween: 10,
			navigation: {
				nextEl: '.next01',
				prevEl: '.prev01',
			},
			breakpoints: {
				991: {
					slidesPerView: 3
				},
				767: {
					slidesPerView: 2
				},
				479: {
					slidesPerView: 1
				}
			}
		});

		new Swiper(revSlider, {
			slidesPerView: 4,
			spaceBetween: 40,
			autoplay: {
				delay: 5000
			},
			breakpoints: {
				1199: {
					slidesPerView: 3,
					spaceBetween: 25
				},
				1050: {
					slidesPerView: 2
				},
				991: {
					slidesPerView: 3
				},
				479: {
					slidesPerView: 1
				}
			}
		});

		if (galleryMainSlides) {
			galleryMainThumbs.remove();
			galleryMain.addClass('gallery-main--w100');

			new Swiper(galleryMain, {
				spaceBetween: 10,
				grabCursor: true
			});
		} else {
			const galleryThumbs = new Swiper(galleryMainThumbs, {
				slidesPerView: 4,
				direction: 'vertical',
				autoHeight: true,
				freeMode: true,
				observer: true,
				observeParents: true,
				observeSlideChildren: true,
				spaceBetween: 10,
				grabCursor: true,
				breakpoints: {
					991: {
						direction: 'horizontal',
						slidesPerView: 3
					}
				}
			});
	
			new Swiper(galleryMain, {
				spaceBetween: 10,
				grabCursor: true,
				thumbs: {
					swiper: galleryThumbs
				}
			});

			if (wrapSliderThumbs.length > 0) galleryThumbs.slideTo(0);
		}
		
		function initTwoProductsSliders(el) {
			return new Swiper(el, {
				slidesPerView: 2,
				spaceBetween: 30,
				navigation: {
					nextEl: '.next02',
					prevEl: '.prev02',
				},
				breakpoints: {
					991: {
						slidesPerView: 2,
						spaceBetween: 10
					},
					800: {
						slidesPerView: 1
					}
				}
			});
		}
		if (twoProductsSlider.length > 0) initTwoProductsSliders(twoProductsSlider);

		function initProductsSliders(el, next, prev) {
			return new Swiper(el, {
				slidesPerView: 4,
				spaceBetween: 30,
				navigation: {
					nextEl: next,
					prevEl: prev
				},
				breakpoints: {
					1199: {
						spaceBetween: 10,
						slidesPerView: 4
					},
					991: {
						slidesPerView: 3
					},
					767: {
						slidesPerView: 2
					},
					479: {
						slidesPerView: 1
					}
				}
			});
		}
		if (productsSlider.length > 0) initProductsSliders(productsSlider, '.next03', '.prev03');
		if (productWatch.length > 0) initProductsSliders(productWatch, '.next04', '.prev04');
		if (productWatchYpu.length > 0) initProductsSliders(productWatchYpu, '.next05', '.prev05');

	}) ();

	// Init fancyBox
	(function initFyncyBox() {
		let imgWrap = $('#slider-reviews').find('[data-fancybox="gallery"]');

		imgWrap.fancybox({
			keyboard: false,
			arrows: false,
			infobar: false,
			buttons: [
				'close'
			],
			touch: false,
			wheel: false
		});
	}());

	// Get char
	(function getChar() {
		let quantity = $('.input-quantity');

		quantity.on('keypress', initChar);

		function initChar() {
			let chr = getChar(event);

			if(event.ctrlKey || event.altKey || event.metaKey) return;
			if (chr == null) return;
			if (chr < '0' || chr > '9') return false;
		}

		function getChar(event) {	  
			if (event.which != 0 && event.charCode != 0) {
				if (event.which < 32) return null;
				return String.fromCharCode(event.which);
			}
		
			return null;
		}
	}) ();


	// Init popups
	(function initPopups() {
		const btnOpenPopups = $('.js-popup-button');
		const btnClosePopups = $('.js-close-popup');

		function initPopup() {
			btnOpenPopups.on('click', function(e) {
				e.preventDefault();
				$('.popup').removeClass('js-popup-show');
				let popupClass = `.${$(this).attr('data-popup')}`;
				$(popupClass).addClass('js-popup-show');
				hiddenScroll();
			});
			closePopup();
		}
	
		function closePopup() {
			btnClosePopups.on('click', function(e) {
				e.preventDefault();
				$('.popup').removeClass('js-popup-show');
				visibleScroll();
			});
		}
		initPopup();
	}) ();


	// Init tabs
	(function initTabs() {
		let wrap = $('#tabs-wrap');
		let tabs = wrap.find('.tabs');
		const tabsItem = tabs.find('li');
		const tabsContent = wrap.find('.tab-content__item');
		let i;

		tabs.on('click', 'li', function(e) {
			e.preventDefault();
			let $this = $(this);
			i = $this.index();

			tabsItem.removeClass('active');
			$this.addClass('active');
			tabsContent.removeClass('active').eq(i).addClass('active');
		});
	}) ();


	// Prepend description
	const prependDesc = (function() {
		let wrapDesc = $('#product-description');
		let allWrap = $('#product-info');
		let insertBackWrap = allWrap.find('.section-product__info-item--insert');
		let insertWrap = $('#tabs-wrap').find('.tab-content__item--descroption');
		let resizeDesc = true;

		function resizeAppendDesc() {
			let maxWidth = window.matchMedia('screen and (max-width: 1199px)').matches;

			if (maxWidth && resizeDesc) {
				insertWrap.prepend(wrapDesc);
				resizeDesc = false;
			} else if (!maxWidth && !resizeDesc) {
				insertBackWrap.prepend(wrapDesc);
				resizeDesc = true;
			}
		}

		return {
			resizeAppendDesc: resizeAppendDesc
		};
	}) ();

	if (document.getElementById('product-description') !== null) prependDesc.resizeAppendDesc();


	// Append block-call
	const appendBlockCall = (function() {
		let list = $('#list');
		const items = list.find('.work-item--card');
		const size = 7;
		const getItems = items.slice(0, size);
		let itemsLength = getItems.length - 1;
		let itemCall = $('#call-wrap');

		function mtRand(min, max) {
			return Math.floor(Math.random() * (max - min + 1) + min);
		}

		function appendRandom() {
			if (itemsLength > 0) {
				const random = mtRand(0, itemsLength);

				getItems.each(function(i, item) {
					if (i === random) return item.before(itemCall[0]);
				});
			}
		}

		return {
			appendRandom: appendRandom
		};
	}) ();

	if (document.getElementById('call-wrap') !== null) appendBlockCall.appendRandom();

	
	// Resize fn
	(function resizeFn() {
		let doit; 

		function resized() {
			initMenu.resizeInitMenu();
			if (document.getElementById('production') !== null)	appendProdTexts.resizeProdTexts();
			if (document.getElementById('product-description') !== null) prependDesc.resizeAppendDesc();
		}

		window.onresize = function() { 
			clearTimeout(doit); 
			doit = setTimeout(function() { 
				resized(); 
			}, 25); 
		};
	}) ();
});